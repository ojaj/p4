/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * https://www.computerhope.com/unix/utr.htm
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 30
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.
#include <map>
using std::map;
using std::getline;

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */
	for (size_t i=0; i<s.length(); i++)
	{
		if ( (s[i] == '\\') && (i+1)<s.length()) //check if it is a backlash and  the backslash is not the last character
		{
			s[i] = 255;
			if (s[i+1]=='n')// becasue \n is a newline.
			{
				s[i+1] = 10;
			}
			else if (s[i+1]=='\\')//becaseu \\is \, 92 assign to \,
			{
				s[i+1] = 92;
			}
			else if (s[i+1]=='t') //bause \t is tab,
			{
				s[i+1] = 9;
			}
			else
			{
				s[i+1] = 255;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);

	/* TODO: finish this... */
	//cout << "s1=" << s1;
	//cout << "\ns2=" << s2;
	//cout << endl;
	map<char,char> m;
	//map<int,int> m;

	if (comp == 0 && del == 0) //when no -c and -d
	{
		for (size_t i=0; i<s1.length(); i++)
		{
			if (i < s2.length())
			{
				m[s1[i]] = s2[i];
			}
			else  //second is shorter than the first string
			{
				m[s1[i]] =  s2[s2.length()-1]; //the last charcter from the second string map to extral character from first string
			}
		}
	}

	if (comp == 1 && del == 0) //-c opetion is here.it is defined see above.
	{
		for (size_t i=0; i<s1.length(); i++)//take care of the first set. that is the first set is not changed.
		{
			m[s1[i]] = s1[i];
		}
		for (size_t i=0; i<128; i++)//take care of the second set. 
		{
			// Found
			if (m.find(i) != m.end())
			{
				continue;
			}
			if (i < s2.length())
			{
				m[i] = s2[i];
			}
			else
			{
				m[i] =  s2[s2.length()-1];
			}
		}
	}


	int linecounter = 0;  
	if (del == 1) //-d is here
	{
		for (size_t i=0; i<s1.length(); i++) //input is put into  the map
		{
			m[s1[i]] = s1[i];
		}

		string input;
		while (getline(cin,input))
		{
			if (linecounter > 0 && comp == 0)
			{
				cout << endl;
			}
			linecounter ++;
			for (size_t i=0; i<input.length(); i++)
			{
				if (comp == 0)
				{
					if (m.find(input[i]) == m.end())		// Cannot find from map
					{
						cout << input[i];
					}
				}
				else
				{
					if (m.find(input[i]) != m.end())		// Find in the map
					{
						cout << input[i];
					}
				}
			}
		}
		return 0;
	}

	//for (map<char,char>::iterator i = m.begin(); i != m.end(); i++) {


	//for (map<int,int>::iterator i = m.begin(); i != m.end(); i++) {
		/* NOTE: each node (each *i) has *two* parts.  They are called
		 * ".first" and ".second" */
		//cout << (*i).first <<"\t" << ((*i).first) << ":\t" << ((*i).second) << "\n";
		/* NOTE: datatype of (*i) is pair<string,int> */
		/* TODO: try to make sense out of the error message for this: */
		// cout << *i << endl;
	//}


	string input;
	cin.clear();
	linecounter = 0;
	while(getline(cin,input)) //print except -d
	{
		if (linecounter > 0 && comp == 0)
		{
			cout << endl;
		}
		for (size_t i=0; i<input.length(); i++)
		{
			// Found
			if (m.find(input[i]) != m.end())
			{
				//cout << "found " << input[i] << endl;
				cout << m[input[i]];
			}
			else
			{
				//cout << "not foound " << input[i] << endl;
				cout << input[i];
			}
		}
		if (linecounter > 0 && comp == 1)
		{
			cout << m[10];
		}
		linecounter ++;
	}

	return 0;
}
